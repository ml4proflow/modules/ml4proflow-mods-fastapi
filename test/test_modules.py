import unittest
from ml4proflow import modules
from ml4proflow_mods.fastapi.modules import FastAPIGraph

class TestModulesModule(unittest.TestCase):
    def setUp(self):
        self.dfm = modules.DataFlowManager()
        self.dfm.create_channel("FastAPI")
    
    def test_create_FastAPIGraph(self):
        dut = FastAPIGraph({"modules": [{"module_ident": "ml4proflow.modules.SourceModule", "module_config": {"channels_push": ["FastAPI"]}}]})
        self.assertIsInstance(dut, FastAPIGraph)

    # define several tests


if __name__ == '__main__':
    unittest.main()
