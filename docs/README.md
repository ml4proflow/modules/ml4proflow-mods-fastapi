# Documentation
***ml4proflow*** uses *[Sphinx](https://www.sphinx-doc.org/en/master/)* for generating code documentation.
> Sphinx is a tool that makes it easy to create intelligent and beautiful documentation, written by Georg Brandl and licensed under the BSD license.
> 
> It was originally created for the Python documentation, and it has excellent facilities for the documentation of software projects in a range of languages. Of course, this site is also created from reStructuredText sources using Sphinx!
# Setup
## Prerequisites
If the recommended template is used, the ```setup.py``` is already correct configured. It should contain the key *extras_require*. An example is given below:
```python 
extras_require={
   "tests": ["pytest", 
            "pandas-stubs",
            "pytest-html",
            "pytest-cov",
            "flake8",
            "mypy"],
   "docs": ["sphinx", "sphinx-rtd-theme", "mr2r2"],
   }
```
Install the dependencies via pip inside the root folder of the ```setup.py``` 
```bash
$ pip install .[docs]
```
> For automatically generate the documentation, the following packages neet to be installed inside your python environment:
> - [Sphinx](https://pypi.org/project/Sphinx/)
> - [sphinx-rtd-theme](https://pypi.org/project/sphinx-rtd-theme/)
> - [m2r2](https://pypi.org/project/m2r2/)

# Generate Doucmentation
## Initialize Sphinx
To initialize *Sphinx* and set up the document layout, run the quickstart-comment of Sphinx. This will initialize the layout inside the folder ```/docs```. Follow the instructions in the terminal for the correct configuration.
```bash
$ sphinx-quickstart docs
```
> Set the meta data according to your module.
> Seperate the source and build directories.

After the last question, new conten is placed inside the folder ```/docs```. The new folder structure should be similiar to this one:
``` 
docs
└── build
└── source
|   └── _static
|   └── _templates
│   │   conf.py
│   │   index.rst
│   make.bat
│   Makefile
│   README.md
```
## Configure document layout
After the initialization, the newly generated files need to be adapt according to the layout and the structure of the framework, including the python script ```conf.py```. This file is called the *build configuration file* and contains almost all settings to custimize the generated documentation. 

First, you need to set the correct path to your source code so that Sphinx can find your modules. 
To do this, modify the following lines in the path setup section: 
```python
# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('../../src/ml4proflow_mods/'))
```
The next section in the configuration file contains information about the project. This lines are automatically filled according to the answers during the initialization of Sphinx. 
> Follow the instructions in the terminal for the correct configuration.
These lines lines do not need to be modified, but you should checked, if the present information are correct and complete. 

In the following section *General configuration* you will find some information about the configuration of Sphinx.
Here you should insert the extension for including documentation from docstrings and the extension to support markdown for the ``README.md``.
Therefore extend the list of extensions with ```'sphinx.ext.autodoc'```.
```python 
extensions = [
   'sphinx.ext.autodoc',
   'm2r2',
]
```

Additionally, it is necessary to tell ``Sphynx`` that there are other file extensions than *.rst*:
```python 
source_suffix = {
    '.rst': 'restructuredtext',
    '.md': 'markdown',
}
```

To generate the documentation in the same theme as the main fraimwork *ml4proflow*, change the ```html_theme``` option in the section *Options for HTML output*.
 ```python 
html_theme = 'sphinx_rtd_theme'
``` 
## Generate and configrure source files
After the layout has been defined, the source files must be generated using ``sphinx-apidoc``. By the use of the **autodoc** extensions, it will generate the source files depending on the *docstrings* of your module. 
To do so, run the following comment inside the *root-folder* of your module and replace <module_name> with the name of the module: 
```bash
$ sphinx-apidoc -o docs/source/ src/ml4proflow_mods/<module_name>
```
The generated source files are placed at ``docs/source`` beside the ``index.rst`` and contains the documentation of all files inside ``src/``. 

Next, to include the generated source files and the ``README.md`` to the documentation, the root documentation has to be configured.
For this purpose, open the file ``index.rst`` and instert following lines above the **toctree** to include the *README*:
```rst 
.. mdinclude:: ../../README.md 
```
Inside the **toctree**, it is necessary to add the generated source file(s): 
```rst 
.. toctree::
   :maxdepth: 2
   :caption: Contents:

   <module_name>
```
## Build documentation
The final step is to build the docmentation. Therefore, the ``setup.py`` of the module provides the necessary command.

> If the recommended template is used, the ```setup.py``` is already correct configured. 

Inside the *root folder*, run: 
```bash
$ python setup.py build_sphinx 
```

Finally, the folder structure of your documentation directory should be similiar to this:

```
docs
└── build
|    └── doctrees
|    └── html
|    |    ├── index.html
|    |    ├── ...
└── source
|    └── _static
|    └── _templates
|    ├── conf.py
|    ├── index.rst
|    ├── ...
├── make.bat
├── Makefile
│   README.md
```

# Viewing the documenation
The generated documentation is available by opening the *html-file* ``index``.
