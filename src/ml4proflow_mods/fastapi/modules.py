from __future__ import annotations  # For class references before definition
from typing import Any, Union
from ml4proflow.modules import DataFlowManager, DataFlowGraph, Module, SourceModule, ExecutableModule, SinkModule # Should be updated, if other classes are used
from ml4proflow import module_finder  # noqa: E402

from fastapi import FastAPI, APIRouter
import uvicorn

import pandas as pd



class FastAPIGraph(SinkModule):
    #def __init__(self, dfm: DataFlowManager, config: dict[str, Any]):
    def __init__(self, config: dict[str, Any]):
        self.config = config
        self.dfm = DataFlowManager()
        self.dfg = DataFlowGraph(self.dfm, self.config)
        SinkModule.__init__(self, self.dfm, self.config)
        self.dfm.register_sink("FastAPI", self)
        #modules.append(FastAPIGraph)
        #print(self.df.modules)
        
        self.router = APIRouter()
        self.router.add_api_route("/run", self.run, methods=["GET"])
        self.router.add_api_route("/get_config", self.get_config, methods=["GET"])
        self.router.add_api_route("/update_config", self.update_config, methods=["POST"])
    
    async def get_config(self):
        return self.config
    
    async def update_config(self, graph_desc: dict) -> dict[str, Any]:
        #config = await self.get_config()
        modules_indexed = {module['module_ident']: module for module in self.config['modules']}
    
        # Iterate over the new configuration modules to update the old configuration
        for new_module in graph_desc['modules']:
            module_ident = new_module['module_ident']

            # If the module exists in the old config, update its config
            if module_ident in modules_indexed:
                # Get the existing module's config
                existing_module_config = modules_indexed[module_ident]['module_config']
                # Update with new values from the new config
                for key, value in new_module['module_config'].items():
                    existing_module_config[key] = value
            else:
                # If the module does not exist, add it to the old config
                self.config['modules'].append(new_module)
                
        #self.dfg = DataFlowGraph(dfm, config)
        self.dfm = DataFlowManager()
        self.dfg = DataFlowGraph(self.dfm, self.config)
        self.dfm.register_sink("FastAPI", self)
        
        return self.config
                            
                        
    async def run(self):
        self.data = pd.DataFrame()
        
        try:
            self.dfg.execute_once()
        except Exception as e:
            # Could raise an http error
            print(f"Got error: {str(e)}")
        
        DataFlowManager()
        return self.data.to_dict()
        
    def on_new_data(self, name: str, sender: SourceModule,
                    data: DataFrame) -> None:
        """Called when there is new data available ready to process
        :param name: Name of the channel
        :param sender: Name of the module that sends the data
        :param data: DataFrame to be processed
        :return:
        """
        print(data)
        self.data = data

    def on_shutdowning(self) -> None:
        for m in self.modules:
            m.on_shutdowning()

    def on_shutdowned(self) -> None:
        for m in self.modules:
            m.on_shutdowned()

    def shutdown(self) -> None:
        self.on_shutdowning()
        self.on_shutdowned()
        



if __name__ == '__main__':
    app = FastAPI()
    
    graph_desc = {
        "modules": [{
            "module_ident": "ml4proflow_mods.influxdb.modules.influxdbSourceModule",
            "module_config": {
                "influxdb_token": "u9NIcE2cYxxdaIUUHW-5tegmGtEqfHO758MNOjI8H88f_RMBh6uVooPTeeK6zrZq6Nee6pYKPEOPb49CiKQfIg==",
                "query_measurementID": "2024-04-10 15:15:54.415303",
                "query_filter_amiro": "amiro12.dhcp.techfak.uni-bielefeld.de",
                "channels_push": ["data"],
                }
            },
            {
                "module_ident": "ml4proflow_mods.LDM.hmi_24.modules.TransformerModule",
                "module_config": {
                    "channels_pull": [
                        "data"
                    ],
                    "channels_push": [
                        "data_transformed"
                    ]
                }
            },
            {
                "module_ident": "ml4proflow_mods.LDM.pipeline_execution.modules.ExecutePipeline",
                "module_config": {
                    "channels_pull": [
                        "data_transformed"
                    ],
                    "channels_push": [
                        "FastAPI"
                    ],
                    "topic_output": "AnomalyDetection_status"
                }
            }
        ]
    }
    
    dfm = DataFlowManager()
    #dfg = FastAPIGraph(dfm, graph_desc)
    dfg = FastAPIGraph(graph_desc)
    
    app.include_router(dfg.router)
    
    uvicorn.run(app, host="0.0.0.0", port=15041)

        